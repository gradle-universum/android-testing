Android Testing Plugin
===============

[![CircleCI](https://circleci.com/bb/gradle-universum/android-testing.svg?style=shield)](https://circleci.com/bb/gradle-universum/android-testing)
[![Codecov](https://codecov.io/bb/gradle-universum/android-testing/branch/main/graph/badge.svg)](https://codecov.io/bb/gradle-universum/android-testing)
[![Codacy](https://api.codacy.com/project/badge/Grade/cd1dad31b351467db8be4b3409fdc01c)](https://www.codacy.com/app/universum-studios/android-testing?utm_source=gradle-universum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=gradle-universum/android-testing&amp;utm_campaign=Badge_Grade)
[![Gradle](https://img.shields.io/badge/gradle-5.6-blue.svg)](https://gradle.com)

Gradle plugin dedicated to testing process of an Android application.

For more information please visit the **[Wiki](https://bitbucket.org/gradle-universum/android-testing/wiki)**.

## Apply ##
[![Bintray](https://api.bintray.com/packages/universum-studios/gradle/universum.studios.gradle%3Aandroid-testing-plugin/images/download.svg)](https://bintray.com/universum-studios/gradle/universum.studios.gradle%3Aandroid-testing-plugin/_latestVersion)

    buildscript {
        dependencies {
            classpath "universum.studios.gradle:android-testing-plugin:${DESIRED_VERSION}"
        }
    }
    
    >>>

    apply plugin: 'universum.studios.android.testing'

## [License](https://bitbucket.org/gradle-universum/android-testing/src/main/LICENSE.md) ##

**Copyright 2018 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.